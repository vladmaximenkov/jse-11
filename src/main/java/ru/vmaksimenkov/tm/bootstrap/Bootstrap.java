package ru.vmaksimenkov.tm.bootstrap;

import ru.vmaksimenkov.tm.api.controller.ICommandController;
import ru.vmaksimenkov.tm.api.controller.IProjectController;
import ru.vmaksimenkov.tm.api.controller.ITaskController;
import ru.vmaksimenkov.tm.api.repository.ICommandRepository;
import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.ICommandService;
import ru.vmaksimenkov.tm.api.service.IProjectService;
import ru.vmaksimenkov.tm.api.service.ITaskService;
import ru.vmaksimenkov.tm.constant.ArgumentConst;
import ru.vmaksimenkov.tm.constant.TerminalConst;
import ru.vmaksimenkov.tm.controller.CommandController;
import ru.vmaksimenkov.tm.controller.ProjectController;
import ru.vmaksimenkov.tm.controller.TaskController;
import ru.vmaksimenkov.tm.repository.CommandRepository;
import ru.vmaksimenkov.tm.repository.ProjectRepository;
import ru.vmaksimenkov.tm.repository.TaskRepository;
import ru.vmaksimenkov.tm.service.CommandService;
import ru.vmaksimenkov.tm.service.ProjectService;
import ru.vmaksimenkov.tm.service.TaskService;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String... args) {
        commandController.showWelcome();
        if (parseArgs(args)) System.exit(0);

        while (true) {
            System.out.println("Enter command: ");
            final String command = TerminalUtil.SCANNER.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP: commandController.showHelp(); break;
            case ArgumentConst.ARG_VERSION: commandController.showVersion(); break;
            case ArgumentConst.ARG_ABOUT: commandController.showAbout(); break;
            case ArgumentConst.ARG_INFO: commandController.showSystemInfo(); break;
            default: incorrectArgument();
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP: commandController.showHelp(); break;
            case TerminalConst.CMD_VERSION: commandController.showVersion(); break;
            case TerminalConst.CMD_ABOUT: commandController.showAbout(); break;
            case TerminalConst.CMD_EXIT: commandController.sysExit(); break;
            case TerminalConst.CMD_INFO: commandController.showSystemInfo(); break;
            case TerminalConst.CMD_COMMANDS: commandController.showCommands(); break;
            case TerminalConst.CMD_ARGUMENTS: commandController.showArguments(); break;

            case TerminalConst.TASK_LIST: taskController.showList(); break;
            case TerminalConst.TASK_CREATE: taskController.create(); break;
            case TerminalConst.TASK_CLEAR: taskController.clear(); break;
            case TerminalConst.TASK_UPDATE_BY_INDEX: taskController.updateTaskByIndex(); break;
            case TerminalConst.TASK_UPDATE_BY_ID: taskController.updateTaskById(); break;
            case TerminalConst.TASK_UPDATE_BY_NAME: taskController.updateTaskByName(); break;
            case TerminalConst.TASK_VIEW_BY_ID: taskController.showTaskById(); break;
            case TerminalConst.TASK_VIEW_BY_NAME: taskController.showTaskByName(); break;
            case TerminalConst.TASK_VIEW_BY_INDEX: taskController.showTaskByIndex(); break;
            case TerminalConst.TASK_REMOVE_BY_ID: taskController.removeTaskById(); break;
            case TerminalConst.TASK_REMOVE_BY_NAME: taskController.removeTaskByName(); break;
            case TerminalConst.TASK_REMOVE_BY_INDEX: taskController.removeTaskByIndex(); break;
            case TerminalConst.TASK_START_BY_ID: taskController.startTaskById(); break;
            case TerminalConst.TASK_START_BY_NAME: taskController.startTaskByName(); break;
            case TerminalConst.TASK_START_BY_INDEX: taskController.startTaskByIndex(); break;
            case TerminalConst.TASK_FINISH_BY_ID: taskController.finishTaskById(); break;
            case TerminalConst.TASK_FINISH_BY_NAME: taskController.finishTaskByName(); break;
            case TerminalConst.TASK_FINISH_BY_INDEX: taskController.finishTaskByIndex(); break;
            case TerminalConst.TASK_SET_STATUS_BY_ID: taskController.setTaskStatusById(); break;
            case TerminalConst.TASK_SET_STATUS_BY_NAME: taskController.setTaskStatusByName(); break;
            case TerminalConst.TASK_SET_STATUS_BY_INDEX: taskController.setTaskStatusByIndex(); break;

            case TerminalConst.PROJECT_LIST: projectController.showList(); break;
            case TerminalConst.PROJECT_CREATE: projectController.create(); break;
            case TerminalConst.PROJECT_CLEAR: projectController.clear(); break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX: projectController.updateProjectByIndex(); break;
            case TerminalConst.PROJECT_UPDATE_BY_ID: projectController.updateProjectById(); break;
            case TerminalConst.PROJECT_UPDATE_BY_NAME: projectController.updateProjectByName(); break;
            case TerminalConst.PROJECT_VIEW_BY_ID: projectController.showProjectById(); break;
            case TerminalConst.PROJECT_VIEW_BY_NAME: projectController.showProjectByName(); break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX: projectController.showProjectByIndex(); break;
            case TerminalConst.PROJECT_REMOVE_BY_ID: projectController.removeProjectById(); break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME: projectController.removeProjectByName(); break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX: projectController.removeProjectByIndex(); break;
            case TerminalConst.PROJECT_START_BY_ID: projectController.startProjectById(); break;
            case TerminalConst.PROJECT_START_BY_NAME: projectController.startProjectByName(); break;
            case TerminalConst.PROJECT_START_BY_INDEX: projectController.startProjectByIndex(); break;
            case TerminalConst.PROJECT_FINISH_BY_ID: projectController.finishProjectById(); break;
            case TerminalConst.PROJECT_FINISH_BY_NAME: projectController.finishProjectByName(); break;
            case TerminalConst.PROJECT_FINISH_BY_INDEX: projectController.finishProjectByIndex(); break;
            case TerminalConst.PROJECT_SET_STATUS_BY_ID: projectController.setProjectStatusById(); break;
            case TerminalConst.PROJECT_SET_STATUS_BY_NAME: projectController.setProjectStatusByName(); break;
            case TerminalConst.PROJECT_SET_STATUS_BY_INDEX: projectController.setProjectStatusByIndex(); break;

            default: incorrectCommand();
        }
    }

    public void incorrectCommand() {
        System.out.println("Unknown command. Type help to see all available commands");
    }

    public void incorrectArgument() {
        System.out.println("Unknown argument. Type -h to see all available arguments");
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
